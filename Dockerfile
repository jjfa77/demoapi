# Imagen base. latest es el nombre del TAG
FROM node:latest

# Directorio de la app
WORKDIR /appJJ

# Copio archivos
ADD . /appJJ

# Dependencias
RUN npm install
RUN apt-get update
RUN apt-get install -y vim

# Puerto
EXPOSE 3000

# Variables de entorno --> Nada

# Comando
CMD ["npm", "start"]
