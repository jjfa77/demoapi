var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//GENERICAS
app.get('/', function(req, res) {
//  res.send('Hola compis');
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req,res){
  res.send('Hemos recibido su petición cambiada por JJ');
});

//CLIENTES
var clientes = [{"idcliente":"1","name": "nombre1","dni": "dni1"}, {"idCliente":"2","name": "nombre2","dni": "dni2"}];

app.get('/clientes', function(req, res) {
  res.json(clientes);

});
app.get('/clientes/:idcliente', function(req, res) {
//  res.send('Aquí tiene al cliente número ' + req.params.idcliente);
  res.json(clientes[req.params.idcliente]);
});

app.post('/clientes', function(req, res) {
  res.sendFile(path.join(__dirname, 'jsonPOST.html'));
});
app.put('/clientes', function(req, res) {
  res.sendFile(path.join(__dirname, 'jsonPUT.html'));
});
app.delete('/clientes', function(req, res) {
  res.sendFile(path.join(__dirname, 'jsonDELETE.html'));
});
